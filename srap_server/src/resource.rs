/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

//! Module providing trait for open resource handlers.

use quinn::{SendStream, RecvStream};

 /// A resource the client has open. New [`Resource`] instances should be created even for different opens of the same resource by the same [`Session`] (this is probably enforced by the borrow checker).
pub trait Resource: Send + 'static {
    /// Talk to the client to figure out exactly what it wants if necessary, then do it if allowable/possible/etc., sending appropriate errors otherwise. The streams talk directly to the client; SRAP is effectively out of the picture once this is called.
    /// The [`Resource`] drops when this method returns, and the streams go with it, which `STOP_SENDING`s the input stream, letting the client know you have stopped accepting input. However, the output and feedback streams will still be finished up; the server will make sure all the data gets to the client if possible, even after [`handle()`](Resource::handle) returns.
    ///
    /// The input stream brings data from the client, the output stream takes data to it, and the feedback stream is used for out-of-band feedback like status and error reports, like standard error/`stderr` on Unix.
    ///
    /// If the client disconnects, reading the input stream will error with [`quinn::ReadError::ConnectionClosed`], and writing the output and feedback streams with [`quinn::WriteError::ConnectionClosed`]. When the client is done sending you input, reading the input stream will error with [`quinn::ReadError::Reset`]; this isn't a failure, and you should continue sending results unless you didn't get all the input you needed (in which case you should send some kind of error on the feedback stream) or the output or feedback streams error with [`quinn::WriteError::Stopped`], which happens when the client closes the resource.
    fn handle(self, input: RecvStream, output: SendStream, feedback: SendStream);
}

#[cfg(test)]
mod tests {
    // TODO: Write some tests.
}
