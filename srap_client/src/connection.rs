/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::session::{Session, SessionID};
use crate::resource::Resource;

use std::{collections::HashSet};
use quinn::{IncomingUniStreams, RecvStream};
use futures::StreamExt;
use uriparse::URI;

pub struct Connection {
    conn: quinn::Connection,
    new_outputs: IncomingUniStreams,
    pending_outputs: Vec<RecvStream>,
    sessions: HashSet<Session>,
    next_session: u64,
}

pub enum OpenError {
    Connection(quinn::ConnectionError),
    Read(quinn::ReadExactError),
    NoNextStream,
    Unknown,
}

impl Connection {
    // TODO: new(), crypto and network setup stuff

    pub fn new_session(&mut self) -> Session {
        let mut num = self.next_session;
        while self.sessions.contains(&Session::with_id(num)) {
            num += 1;
        }
        let sess = Session::with_id(num);
        self.sessions.insert(sess);
        sess
    }
    
    async fn find_output_stream(&mut self, id: u64) -> Result<RecvStream, OpenError> {
        // Check existing output streams and put back the ones that don't match.
        // FIXME: Inefficient.
        let mut new_pending_outputs = Vec::with_capacity(self.pending_outputs.len());
        for stream in self.pending_outputs.drain(0..) {
            if Into::<quinn::VarInt>::into(stream.id()).into_inner() == id {
                return Ok(stream);
            } else {
                new_pending_outputs.push(stream);
            }
        }
        self.pending_outputs = new_pending_outputs;

        // Check new streams until one that matches arrives.
        loop {
            let stream = match self.new_outputs.next().await {
                Some(result) => match result {
                    Ok(stream) => stream,
                    Err(e) => return Err(OpenError::Connection(e)),
                },
                None => return Err(OpenError::NoNextStream)
            };
            if Into::<quinn::VarInt>::into(stream.id()).into_inner() == id {
                return Ok(stream);
            }
        }
    }
    
    pub async fn open(&mut self, session: Session, uri: URI<'static>) -> Result<Resource, OpenError> {
        let (mut input, mut feedback) = match self.conn.open_bi().await {
            Ok(streams) => streams,
            Err(e) => return Err(OpenError::Connection(e)),
        };

        // Send session ID and URI.
        let mut buf = Vec::<u8>::new();
        buf.extend(&session);
        buf.extend(String::from(uri.clone()).into_bytes());
        buf.push(b'\n');
        input.write_all(&buf);
        
        let mut code = [0 as u8; 1];
        match feedback.read_exact(&mut code).await {
            Ok(_) => {},
            Err(e) => return Err(OpenError::Read(e)),
        };
        match code[0] {
            b'S' => {},
            // TODO: Server errors.
            _ => return Err(OpenError::Unknown),
        }
        
        let mut output_id = [8 as u8; 8];
        match feedback.read_exact(&mut output_id).await {
            Ok(_) => {},
            Err(e) => return Err(OpenError::Read(e)),
        };
        let output = self.find_output_stream(u64::from_be_bytes(output_id)).await?;
        
        Ok(Resource{uri: uri, input: input, output: output, feedback: feedback})
    }
}
