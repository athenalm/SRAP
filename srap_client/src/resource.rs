/* SPDX-License-Identifier: AGPL-3.0-or-later
Rust reference implementation of Stream Resource Application Protocol.
Copyright (C) 2021 Alex Martin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use quinn::{SendStream, RecvStream};
use uriparse::URI;

pub struct Resource {
    pub uri: URI<'static>,
    pub input: SendStream,
    pub output: RecvStream,
    pub feedback: RecvStream,
}
