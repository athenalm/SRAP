# Stream Resource Application Protocol (Rust reference implementation)

This is the Rust reference implementation of the Stream Resource Application Protocol (SRAP), a lower-application-layer protocol built on QUIC. SRAP is a simple protocol that lets clients establish multiple sessions within the same QUIC connection, and open multiple resources by URI in each session. Each resource has input and output streams, along with an extra "feedback" stream, like standard error/`stderr` on Unix. The resource opening mechanism is designed to allow relatively simple forward and reverse proxying.

The protocol details have not stabilized, so they are not yet publically documented. While an unpublished specification exists, it is considered an incomplete draft, and does not include byte-level formats.

The reference implementation currently consists only of a largely-implemented server library based on `tokio` and `quinn`. When complete, it will also include a client library capable of multiple simultaneous sessions. A proxy library is under consideration.

The reference implementation is licensed under the GNU Affero General Public License, version 3.0, or any later version (at your option). If you use it to implement a service you make available over a network, users of that service must be able to request its source code. The request mechanism does not have to be "in-band" (i.e. does not have to be possible via SRAP), but it must be at no charge and by some reasonably standard means.
